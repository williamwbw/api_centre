package com.IRISAPIWD.IRISAPIWD.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.IRISAPIWD.IRISAPIWD.Modal.*;

@RestController

public class RESTApi_MiddleMan {

    @RequestMapping("/api/getInformation")
    public String getInformation(String Data) throws Exception {
        return SignData.sign(Data);
    }

    @RequestMapping("/api/verifyInformation")
    public boolean verifyInformation() throws Exception {
        return VerifyData.verifyData(SignData.sign("test"));
    }
}
