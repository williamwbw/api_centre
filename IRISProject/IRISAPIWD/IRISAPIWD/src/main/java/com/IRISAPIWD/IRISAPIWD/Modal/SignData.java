package com.IRISAPIWD.IRISAPIWD.Modal;
import java.security.*;

public class SignData {
    public static String sign(String string) throws Exception {
        
        Signature signature = Signature.getInstance("SHA256WithDSA");
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        signature.initSign(keyPair.getPrivate(), secureRandom);
        byte[] data = string.getBytes("UTF-8");
        signature.update(data);

        byte[] digitalSignature = signature.sign();

        return digitalSignature.toString();
	}
}
