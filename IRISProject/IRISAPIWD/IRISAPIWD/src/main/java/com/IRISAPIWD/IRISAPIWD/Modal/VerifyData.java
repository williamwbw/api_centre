package com.IRISAPIWD.IRISAPIWD.Modal;
import java.security.*;

public class VerifyData {
    public static boolean verifyData(String SignedData) throws Exception {

        Signature signature = Signature.getInstance("SHA256WithDSA");
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        signature.initVerify(keyPair.getPublic());
        byte[] data = SignedData.getBytes("UTF-8");
        signature.update(data);

        boolean verified = signature.verify(data);
        
        return verified;
	}
}
