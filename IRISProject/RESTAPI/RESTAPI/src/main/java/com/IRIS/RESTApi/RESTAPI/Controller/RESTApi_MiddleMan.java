package com.IRIS.RESTApi.RESTAPI.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.IRIS.RESTApi.RESTAPI.Modal.*;

@RestController

public class RESTApi_MiddleMan {

    @RequestMapping("/api/getInformation")
    public String getInformation() throws Exception {
        return SignData.sign("test");
    }

    @RequestMapping("/api/verifyInformation")
    public boolean verifyInformation() throws Exception {
        return VerifyData.verifyData(SignData.sign("test"));
    }
}
