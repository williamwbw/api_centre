﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTTProject.Models;

namespace NTTProject.Areas.Log
{
    public class LogFileController : Controller
    {
        LogFile Log = new LogFile();
        // GET: Log/LogFile
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("areas/StatusUpdate")]
        public string UpdateStatus(int CSID, string RecordStatus, string User, string RefTable)
        {
            return Log.UpdateStatus(CSID, RecordStatus, User, RefTable);
        }
    }
}