﻿using Newtonsoft.Json;
using NTTProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NTTProject.Controllers
{
    public class CameraScreenSetupController : ApiController
    {
        dbController db = new dbController();
        CameraScreenSetup cs = new CameraScreenSetup();
        Users UserCentre = new Users();
        LogFile Log = new LogFile();

        // GET: api/CameraScreenSetup/5
        [HttpGet]
        [Route("api/CameraScreenSetup/Save")]
        public string Save(int cid, string CSStatus, string cstitle, string cssr, int createuserid, int modifyuserid)
        {
            NTT_tblCameraScreen cs = new NTT_tblCameraScreen
            {
                CID = cid,
                CSTitle = cstitle,
                RecordStatus = CSStatus,
                CSScreenResolution = cssr,
                CreateDateTime = DateTime.Now,
                CreateUserID = createuserid,
                ModifyUserID = modifyuserid,
                ModifyDateTime = DateTime.Now,
            };

            db.NTT_tblCameraScreens.InsertOnSubmit(cs);
            db.SubmitChanges();

            return "value";
        }

        [HttpGet]
        [Route("api/CameraScreenSetup/Edit")]
        public string Edit(int csid, int oldCID, int newCID, string CSTitle, DateTime CSStartDateTime, string CSScreenResolution, string User)
        {
            try
            {
                NTT_tblCameraScreen CSSelect = (NTT_tblCameraScreen)(from X in db.NTT_tblCameraScreens
                                                                     where X.CSID == csid && X.CID == oldCID
                                                                        select X).SingleOrDefault();
                if(CSSelect == null)
                {
                    return "No Data Selected!";
                }

                CSSelect.CID = newCID;
                CSSelect.CSTitle = CSTitle;
                CSSelect.CSScreenResolution = CSScreenResolution;
                CSSelect.ModifyUserID = UserCentre.GetUserID(User);
                CSSelect.ModifyDateTime = DateTime.Now;

                db.SubmitChanges();
                return "Saved Successfull!";
            }catch(Exception ex)
            {
                return "Saved Failed!";
            }
        }

        // POST: api/CameraScreenSetup
        [HttpGet]
        [Route("api/CameraScreenSetup/GetAllCameraScreen")]
        public object GetAllCameraScreen(int CSID)
        {
            CameraListing CL = new CameraListing();
            if (CSID > 0)
            {
                CL = (CameraListing)(from s in db.view_CameraScreenInfos
                                     where s.CSID == CSID
                                     select s);
            }
            else
            {
                CL = (CameraListing)(from s in db.view_CameraScreenInfos
                                     select s);
            }

            string json = JsonConvert.SerializeObject(CL);
            return json;
        }

        // PUT: api/CameraScreenSetup/5
        public IHttpActionResult Put(CameraScreenSetup camerascreen)
        {
            return Ok();
        }

        // DELETE: api/CameraScreenSetup/5
        public IHttpActionResult Delete(int csid)
        {
            return Ok();
        }
    }
}
