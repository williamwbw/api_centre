﻿using Newtonsoft.Json;
using NTTProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NTTProject.Controllers
{
    public class CameraSetupController : ApiController
    {
        dbController db = new dbController();
        CameraSetup cs = new CameraSetup();
        Users UserCentre = new Users();

        // GET: api/CameraSetup/5
        [HttpGet]
        [Route("api/CameraSetup/Save")]
        public string Save(string ctitle, string cstatus, int clocation, string curl, int ctype, int createuserid, int modifyuserid)
        {
            NTT_tblCamera a = new NTT_tblCamera
            {
                CTitle = ctitle,
                CStatus = cstatus,
                CLocation = clocation,
                CURL = curl,
                CType = ctype,
                CreateDateTime = DateTime.Now,
                CreateUserID = createuserid,
                ModifyUserID = modifyuserid,
                ModifyDateTime = DateTime.Now,
            };

            db.NTT_tblCameras.InsertOnSubmit(a);
            db.SubmitChanges();

            return "value";
        }

        [HttpGet]
        [Route("api/CameraSetup/Edit")]
        public string Edit(int cid, string CTitle, string cstatus, int clocation, int CType, string curl, string User)
        {
            try
            {
                NTT_tblCamera CSSelect = (NTT_tblCamera)(from X in db.NTT_tblCameras
                                                             where X.CID == cid
                                                             select X).SingleOrDefault();
                if (CSSelect == null)
                {
                    return "No Data Selected!";
                }

                CSSelect.CTitle = CTitle;
                CSSelect.CStatus = cstatus;
                CSSelect.CType = CType;
                CSSelect.CLocation = clocation;
                CSSelect.CURL = curl;
                CSSelect.ModifyUserID = UserCentre.GetUserID(User);
                CSSelect.ModifyDateTime = DateTime.Now;

                db.SubmitChanges();
                return "Saved Successfull!";
            }
            catch (Exception ex)
            {
                return "Saved Failed!";
            }
        }

        // POST: api/CameraSetup
        [HttpGet]
        [Route("api/CameraSetup/GetAllCamera")]
        public object GetAllCamera(int CID)
        {
            var q = from s in db.view_CameraListings
                    where s.CStatus != "Inactive"
                    select new
                    {
                        CID = s.CID,
                        CTitle = s.Title,
                        CLocation = s.CameraLocation,
                        CStatus = s.CStatus,
                        CURL = s.URL,
                        CType = s.CameraType,
                    };
            if (CID > 0)
            {
                q = from s in db.view_CameraListings
                    where s.CID == CID && s.CStatus != "Inactive"
                    select new
                    {
                        CID = s.CID,
                        CTitle = s.Title,
                        CLocation = s.CameraLocation,
                        CStatus = s.CStatus,
                        CURL = s.URL,
                        CType = s.CameraType,
                    };
            }
            string json = JsonConvert.SerializeObject(q);
            return json;
        }

        // PUT: api/CameraSetup/5
        public IHttpActionResult Put(CameraSetup camera)
        {
            return Ok();
        }

        // DELETE: api/CameraSetup/5
        public IHttpActionResult Delete(int cid)
        {
            return Ok();
        }
    }
}
