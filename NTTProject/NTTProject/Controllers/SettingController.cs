﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NTTProject.Controllers
{
    public class SettingController : ApiController
    {
        dbController db = new dbController();

        [HttpGet]
        [Route("api/Setting/getCameraLocation")]
        public object getCameraLocation()
        {
            try
            {
                var CameraLocationJson = from s in db.cf_tblSettings
                                            where s.RecordStatus != "Inactive" & s.Type == "Location"
                                            select new
                                            {
                                                value = s.SettingID,
                                                label = s.Title
                                            };
                string json = JsonConvert.SerializeObject(CameraLocationJson);
                return json;
            }
            catch(Exception ex)
            {

            }
            return "value";
        }

        [HttpGet]
        [Route("api/Setting/getCameraLocationAll")]
        public object getCameraLocationAll()
        {
            try
            {
                var CameraLocationJson = from s in db.cf_tblSettings
                                         where s.RecordStatus != "Inactive"
                                         select new
                                         {
                                             Title = s.Title,
                                             Description = s.Description,
                                             Type = s.Type

                                         };
                string json = JsonConvert.SerializeObject(CameraLocationJson);
                return json;
            }
            catch (Exception ex)
            {

            }
            return "value";
        }
        [HttpGet]
        [Route("api/Setting/getScreenResolution")]
        public object getScreenResolution()
        {
            try
            {
                var ScreenResolutionJson = from s in db.cf_tblSettings
                                         where s.RecordStatus != "Inactive" & s.Type == "Resolution"
                                         select new
                                         {
                                             value = s.SettingID,
                                             label = s.Title
                                         };
                string json = JsonConvert.SerializeObject(ScreenResolutionJson);
                return json;
            }
            catch (Exception ex)
            {

            }
            return "value";
        }

        [HttpGet]
        [Route("api/Setting/getCameraType")]
        public object getCameraType()
        {
            try
            {
                var CameraTypeJson = from s in db.cf_tblSettings
                                         where s.RecordStatus != "Inactive" & s.Type == "API"
                                         select new
                                         {
                                             value = s.SettingID,
                                             label = s.Title
                                         };
                string json = JsonConvert.SerializeObject(CameraTypeJson);
                return json;
            }
            catch (Exception ex)
            {

            }
            return "value";
        }

        [HttpGet]
        [Route("api/Setting/getCameraList")]
        public object getCameraList()
        {
            try
            {
                var CameraListJson = from s in db.view_CameraListings
                                     select new
                                     {
                                         name = s.Title,
                                         code = s.CID
                                     };
                string json = JsonConvert.SerializeObject(CameraListJson);
                return json;
            }
            catch (Exception ex)
            {

            }
            return "value";
        }
    }
}