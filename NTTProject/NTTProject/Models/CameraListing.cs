﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTTProject.Models
{
    public class CameraListing
    {
		private int _CSID;

		private int _CID;

		private string _CSTitle;

		private string _CSScreenResolution;

		private string _RecordStatus;

		private int _CreateUserID;

		private System.DateTime _CreateDateTime;

		private int _ModifyUserID;

		private System.DateTime _ModifyDateTime;

		private int _CType;

		private string _CStatus;

		private int _CLocation;

		private string _CURL;

		private string _CTitle;

		private string _RefTable;

		private System.Nullable<int> _RefRecordID;

		private System.Nullable<System.DateTime> _StartDateTime;

		private System.Nullable<System.DateTime> _EndDateTime;
	}
}