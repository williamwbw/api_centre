﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTTProject.Models
{
    public class CameraScreenSetup
    {
        public int csid { get; set; }
        public int cid { get; set; }
        public string cstitle { get; set; }
        public DateTime csstartdatetime { get; set; }
        public string cssr { get; set; }
        public DateTime createDateTime { get; set; }
        public DateTime modifyDateTime { get; set; }
        public int createuserid { get; set; }
        public int modifyuserid { get; set; }
    }
}