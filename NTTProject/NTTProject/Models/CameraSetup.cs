﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTTProject.Models
{
    public class CameraSetup
    {
        public int cid { get; set; }
        public string ctitle { get; set; }
        public string cstatus { get; set; }
        public string clocation { get; set; }
        public string curl { get; set; }
        public string ctype { get; set; }
        public string createDateTime { get; set; }
        public string modifyDateTime { get; set; }
        public int createuserid { get; set; }
        public int modifyuserid { get; set; }
    }
}