﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTTProject.Models
{
    public class LogFile
    {
		private int _LogID;

		private string _RefTable;

		private int _RefRecordID;

		private System.DateTime _StartDateTime;

		private System.Nullable<System.DateTime> _EndDateTime;

		private string _RecordStatus;

		private int _CreateUserID;

		private System.DateTime _CreateDateTime;

		private int _ModifyUserID;

		private System.DateTime _ModifyDateTime;

        public string UpdateStatus(int CSID, string RecordStatus, string User, string RefTable)
        {
            try
            {
                dbController db = new dbController();
                Users UserCentre = new Users();
                cf_tblLog Log = new cf_tblLog();
                Log = (cf_tblLog)(from X in db.cf_tblLogs
                                  where X.RefRecordID == CSID && X.RefTable == RefTable
                                  select X).SingleOrDefault();

                if (Log == null)
                {
                    return "No Exists Data To Update!";
                }

                Log.RecordStatus = RecordStatus;
                Log.ModifyUserID = UserCentre.GetUserID(User);
                Log.ModifyDateTime = DateTime.Now;

                db.SubmitChanges();

                return "Update Successful!";
            }
            catch (Exception ex)
            {
                return "Update Failed";
            }
        }
    }
}