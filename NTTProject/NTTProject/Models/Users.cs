﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTTProject.Models
{
    public class Users
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PermissionRole { get; set; }
        public string RecordStatus { get; set; }

        public int GetUserID(string UserName)
        {
            dbController db = new dbController();
            int UserID = 0;
            try
            {
                cf_tbluser user = new cf_tbluser();
                user = (cf_tbluser)(from X in db.cf_tblusers
                                    where X.UserName == UserName && X.RecordStatus != "Inactive"
                                    select X).SingleOrDefault();
                UserID = user.UserId;
            }
            catch (Exception ex)
            {
            }
            return UserID;
        }
    }
}