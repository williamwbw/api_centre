USE [NTTCameraCentre]
GO
/****** Object:  Table [dbo].[cf_tblRole]    Script Date: 11/1/2021 下午 6:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cf_tblRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsSystem] [bit] NOT NULL,
	[RecordStatus] [nvarchar](50) NOT NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
	[ModifyDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[cf_tblRole] ADD  DEFAULT ('N') FOR [IsSystem]
GO
ALTER TABLE [dbo].[cf_tblRole] ADD  DEFAULT ('Active') FOR [RecordStatus]
GO
ALTER TABLE [dbo].[cf_tblRole] ADD  DEFAULT (getdate()) FOR [CreateDateTime]
GO
ALTER TABLE [dbo].[cf_tblRole] ADD  DEFAULT (getdate()) FOR [ModifyDateTime]
GO
