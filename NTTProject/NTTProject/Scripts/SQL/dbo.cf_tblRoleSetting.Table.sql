USE [NTTCameraCentre]
GO
/****** Object:  Table [dbo].[cf_tblRoleSetting]    Script Date: 11/1/2021 下午 6:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cf_tblRoleSetting](
	[RoleSettingID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[SystemSetting] [bit] NOT NULL,
	[AddCamera] [bit] NOT NULL,
	[EditCamera] [bit] NOT NULL,
	[ViewCamera] [bit] NOT NULL,
	[DeleteCamera] [bit] NOT NULL,
	[AddCameraScreen] [bit] NOT NULL,
	[EditCameraScreen] [bit] NOT NULL,
	[ViewCameraScreen] [bit] NOT NULL,
	[DeleteCameraScreen] [bit] NOT NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
	[ModifyDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleSettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
