USE [NTTCameraCentre]
GO
/****** Object:  Table [dbo].[cf_tblLog]    Script Date: 11/1/2021 下午 6:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cf_tblLog](
	[LogID] [int] NOT NULL,
	[RefTable] [nvarchar](200) NOT NULL,
	[RefRecordID] [int] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[RecordStatus] [nvarchar](50) NOT NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
	[ModifyDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
