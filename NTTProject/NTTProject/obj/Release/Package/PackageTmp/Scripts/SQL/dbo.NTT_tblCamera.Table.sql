USE [NTTCameraCentre]
GO
/****** Object:  Table [dbo].[NTT_tblCamera]    Script Date: 11/1/2021 下午 6:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NTT_tblCamera](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[CType] [int] NOT NULL,
	[CStatus] [nvarchar](50) NOT NULL,
	[CLocation] [int] NOT NULL,
	[CURL] [nvarchar](max) NULL,
	[CTitle] [nvarchar](max) NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
	[ModifyDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
