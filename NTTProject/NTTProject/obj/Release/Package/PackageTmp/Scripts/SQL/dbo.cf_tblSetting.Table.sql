USE [NTTCameraCentre]
GO
/****** Object:  Table [dbo].[cf_tblSetting]    Script Date: 11/1/2021 下午 6:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cf_tblSetting](
	[SettingID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[RecordStatus] [nvarchar](50) NOT NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifyUserID] [int] NOT NULL,
	[ModifyDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
